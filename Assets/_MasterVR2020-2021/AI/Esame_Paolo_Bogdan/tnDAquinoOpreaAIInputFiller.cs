﻿using UnityEngine;
using BehaviorDesigner.Runtime;
using TuesdayNights;
using DaqOpr_SharedVariables;
using System;

public class tnDAquinoOpreaAIInputFiller : tnStandardAIInputFillerBase
{

    private AIRole m_Role = AIRole.Null;

    #region var added by us
    private GameObject m_Self;
    private BehaviorTree bt = null;

    private InputGameData m_data = new InputGameData();
    private GameTimers m_timers = new GameTimers();
    
    #endregion
    private float m_SmoothTime = 0.0f;

    // STATIC VARIABLES

    private static string s_Params = "Data/AI/AIParams";

    // tnInputFiller's INTERFACE

    public override void Fill(float i_FrameTime, tnInputData i_Data)
    {
        if (!initialized || self == null)
        {
            ResetInputData(i_Data);
            return;
        }

        if (m_Role == AIRole.Null)
        {
            ResetInputData(i_Data);
            return;
        }

        UpdateInputData(i_FrameTime);

        // Fill input data.
        if (bt.GetVariable("OutputGameData").GetValue() is OutputGameData output)
        {
            UpdateTimers(output, i_FrameTime);

            var m_Axes = output.axes;
            var requestKick = output.kickButton || output.tackleRequested;

            i_Data.SetAxis(InputActions.s_HorizontalAxis, m_Axes.x);
            i_Data.SetAxis(InputActions.s_VerticalAxis, m_Axes.y);

            i_Data.SetButton(InputActions.s_PassButton, requestKick);
            i_Data.SetButton(InputActions.s_ShotButton, output.dashButton);

            i_Data.SetButton(InputActions.s_TauntButton, output.tauntButton);
            i_Data.SetButton(InputActions.s_AttractButton, output.isAttracting);
        }
    }
   public override void Setup(tnBaseAIData i_Data)
    {
        base.Setup(i_Data);

        //SETUP STATIC GLOBAL DATA
        m_data.ball = ball;
        m_data.ballRadius = ballRadius;

        m_data.teamCharactersCount = teamCharactersCount;
        m_data.teammatesCount = teammatesCount;
        m_data.opponentsCount = opponentsCount;
        m_data.myGoal = myGoal;
        m_data.opponentGoal = opponentGoal;

        m_data.fieldWidth = fieldWidth;
        m_data.fieldHeight = fieldHeight;

        m_data.gkAreaMinHeight = gkAreaMinHeight;
        m_data.gkAreaMaxHeight = gkAreaMaxHeight;
        m_data.gkAreaWidth = gkAreaWidth;
        m_data.gkAreaHeight = gkAreaHeight;

        m_data.goalWidth = goalWidth;
        m_data.goalMaxHeight = goalMaxHeight;
        m_data.goalMinHeight = goalMinHeight;

        m_data.colliderRadius = colliderRadius;

       m_data.m_Opponents = opponents;
       m_data.m_Teams = teams;
    }
    private void UpdateTimers(OutputGameData output, float i_FrameTime)
    {
        UpdateCooldownTimers(i_FrameTime);
        UpdateRecoverTimer(i_FrameTime);
        UpdateAttractTimer(output, i_FrameTime);

        bt.SetVariableValue("GameTimers", m_timers);
    }

    private void UpdateInputData(float i_FrameTime)
    {
        m_data.frameTime = i_FrameTime;

        m_data.myPosition = myPosition;

        m_data.isEnergyEnoughToKick = CheckEnergy(m_data.m_MinKickEnergy);
        m_data.isEnergyEnoughToAttract = CheckEnergy(m_data.m_MinAttractEnergy);
        m_data.isEnergyEnoughToTackle = CheckEnergy(m_data.m_MinTackleEnergy);
        m_data.isEnergyEnoughToDash = CheckEnergy(m_data.m_MinDashEnergy);

        m_data.ballDistance = ballDistance;
        m_data.referencePos = referencePosition;

        m_data.topLeft = topLeft;
        m_data.topRight = topRight;
        m_data.bottomLeft = bottomLeft;
        m_data.bottomRight = bottomRight;

        m_data.ballPosition = ball != null ? ball.position : Vector3.zero;

        m_data.midfield = midfield;

        bt.SetVariableValue("InputGameData", m_data);
    }

    public override void Clear()
    {
        bt.SetVariableValue("OutputGameData", new OutputGameData());
        bt.SetVariableValue("GameTimers", new GameTimers());
    }

    private void UpdateCooldownTimers(float i_FrameTime)
    {
        if (m_timers.kickCooldownTimer > 0f)
        {
            m_timers.kickCooldownTimer -= i_FrameTime;

            if (m_timers.kickCooldownTimer < 0f)
            {
                m_timers.kickCooldownTimer = 0f;
            }
        }

        if (m_timers.dashCooldownTimer > 0f)
        {
            m_timers.dashCooldownTimer -= i_FrameTime;

            if (m_timers.dashCooldownTimer < 0f)
            {
                m_timers.dashCooldownTimer = 0f;
            }
        }

        if (m_timers.tackleCooldownTimer > 0f)
        {
            m_timers.tackleCooldownTimer -= i_FrameTime;

            if (m_timers.tackleCooldownTimer < 0f)
            {
                m_timers.tackleCooldownTimer = 0f;
            }
        }

        if (m_timers.attractCooldownTimer > 0f)
        {
            m_timers.attractCooldownTimer -= i_FrameTime;

            if (m_timers.attractCooldownTimer < 0f)
            {
                m_timers.attractCooldownTimer = 0f;
            }
        }
    }
    private void UpdateRecoverTimer(float i_FrameTime)
    {
        float ballSpeed = GetVehicleSpeed(m_data.ball.gameObject);
        float mySpeed = GetVehicleSpeed(self);

        if (m_data.ballDistance < m_data.m_RecoverRadius && (ballSpeed < 0.15f && mySpeed < 0.15f))
        {
            m_timers.recoverTimer += i_FrameTime;
        }
        else
        {
            m_timers.recoverTimer = 0f;
        }
    }
    private void UpdateAttractTimer(OutputGameData outputData,float i_FrameTime)
    {
        if (outputData.isAttracting)
        {
            m_timers.attractTimer += i_FrameTime;
        }
        else
        {
            m_timers.attractTimer = 0;
        }
    }

    //ctor
    public tnDAquinoOpreaAIInputFiller(GameObject i_Self, AIRole i_Role)
        : base(i_Self)
    {
        m_Self = i_Self;
        bt = m_Self.GetComponent<BehaviorTree>();

        m_Role = i_Role;
        bt.SetVariableValue("CharacterRole", m_Role);
        bt.SetVariableValue("Self", self);
        bt.SetVariableValue("OutputGameData", new OutputGameData());
        bt.SetVariableValue("InputGameData", new InputGameData());
        bt.SetVariableValue("GameTimers", new GameTimers());

        // Setup parameters.

        tnStandardAIInputFillerParams aiParams = Resources.Load<tnStandardAIInputFillerParams>(s_Params);

        if (aiParams == null)
        {
            Debug.LogWarning("AI Params is null");
            return;
        }

        // Seek-and-flee behaviour.

        aiParams.GetMinFleeDistanceFactor(out m_data.m_MinFleeDistanceFactor);
        aiParams.GetMaxFleeDistanceFactor(out m_data.m_MaxFleeDistanceFactor);

        // Separation.

        aiParams.GetSeparationThreshold(out m_data.m_SeparationThreshold);

        // Energy thresholds.

        aiParams.GetMinDashEnergy(out m_data.m_MinDashEnergy);
        aiParams.GetMinKickEnergy(out m_data.m_MinKickEnergy);
        aiParams.GetMinTackleEnergy(out m_data.m_MinTackleEnergy);
        aiParams.GetMinAttractEnergy(out m_data.m_MinAttractEnergy);

        // Cooldown timers.

        aiParams.GetDashCooldown(out m_data.m_DashCooldown);
        aiParams.GetKickCooldown(out m_data.m_KickCooldown);
        aiParams.GetTackleCooldown(out m_data.m_TackleCooldown);
        aiParams.GetAttractCooldown(out m_data.m_AttractCooldown);

        // Dash behaviour.

        aiParams.GetDashDistance(out m_data.m_DashDistance);
        aiParams.GetForcedDashDistance(out m_data.m_ForcedDashDistance);

        // Kick behaviour.

        aiParams.GetKickPrecision(out m_data.m_KickPrecision);

        // Tackle behaviour.

        aiParams.GetTackleRadius(out m_data.m_TackleRadius);
        aiParams.GetBallDistanceThreshold(out m_data.m_BallDistanceThreshold);

        // Attract behaviour.

        aiParams.GetAttractMinRadius(out m_data.m_AttractMinRadius);
        aiParams.GetAttractMaxRadius(out m_data.m_AttractMaxRadius);

        aiParams.GetAttractTimeThreshold(out m_data.m_AttractTimeThreshold);

        // Extra parameters.

        aiParams.GetRecoverRadius(out m_data.m_RecoverRadius);
        aiParams.GetRecoverTimeThreshold(out m_data.m_RecoverTimeThreshold);

        UpdateInputData(0);

        bt.EnableBehavior();
    }
}
