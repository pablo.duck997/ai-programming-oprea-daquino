﻿using System;
using BehaviorDesigner.Runtime;

namespace DaqOpr_SharedVariables
{
    [Serializable]
    public class GameTimers
    {
        public float kickCooldownTimer = 0f;
        public float dashCooldownTimer = 0f;
        public float tackleCooldownTimer = 0f;
        public float attractCooldownTimer = 0f;

        public float recoverTimer = 0f;
        public float attractTimer = 0f;
    }

    [Serializable]
    public class DaqOprGameTimers : SharedVariable<GameTimers>
    {
        public override string ToString() { return mValue == null ? "null" : mValue.ToString(); }
        public static implicit operator DaqOprGameTimers(GameTimers value) { return new DaqOprGameTimers { mValue = value }; }
    }
}