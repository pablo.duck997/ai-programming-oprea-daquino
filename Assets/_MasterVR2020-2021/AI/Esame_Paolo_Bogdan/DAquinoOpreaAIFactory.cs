﻿using System.Collections;
using System.Collections.Generic;
using TuesdayNights;
using BehaviorDesigner.Runtime;
using UnityEngine;

public class DAquinoOpreaAIFactory : tnBaseStandardMatchAIFactory
{
#region AIRole
    private static AIRole[] s_Roles_1 = new AIRole[]
    {
        AIRole.Midfielder,
    };

    private static AIRole[] s_Roles_2 = new AIRole[]
    {
        AIRole.Midfielder,
        AIRole.Midfielder,
    };

    private static AIRole[] s_Roles_3 = new AIRole[]
    {
        AIRole.Midfielder,
        AIRole.Midfielder,
        AIRole.Midfielder,
    };

    private static AIRole[][] s_Roles = new AIRole[][]
    {
        s_Roles_1,
        s_Roles_2,
        s_Roles_3,
    };
    
#endregion
    
    private static AIRole s_DefaultRole = AIRole.Midfielder;
    private List<AIRole> m_Roles = null;
    private int m_AICreated = 0;
    protected override void OnConfigure(tnTeamDescription i_TeamDescription)
    {
        base.OnConfigure(i_TeamDescription);

        if (i_TeamDescription == null)
            return;

        int charactersCount = i_TeamDescription.charactersCount;

        if (charactersCount <= 0 || charactersCount > s_Roles.Length)
            return;

        AIRole[] roles = s_Roles[charactersCount - 1];

        if (roles == null || roles.Length == 0 || roles.Length != charactersCount)
            return;

        int aiIndex = 0;

        for (int characterIndex = 0; characterIndex < charactersCount; ++characterIndex)
        {
            tnCharacterDescription characterDescription = i_TeamDescription.GetCharacterDescription(characterIndex);

            if (characterDescription == null)
                continue;

            int playerId = characterDescription.playerId;
            tnPlayerData playerData = tnGameData.GetPlayerDataMain(playerId);

            if (playerData == null)
            {
                AIRole role = roles[aiIndex++];
                
                m_Roles.Add(role);
            }
        }

        m_Roles.Sort();
    }

    protected override tnStandardAIInputFillerBase OnCreateAI(int i_Index, GameObject i_Character)
    {
        //return base.OnCreateAI(i_Index, i_Character);
        if (m_Roles.Count == 0 || m_AICreated >= m_Roles.Count)
        {
            return CreateInputFiller(s_DefaultRole, i_Character);
        }

        AIRole role = m_Roles[m_AICreated++];
        
        return CreateInputFiller(role, i_Character);
    }
    private tnStandardAIInputFillerBase CreateInputFiller(AIRole i_Role, GameObject i_Character)
    {
        //return new tnStandardAIInputFillerNull(i_Character);
        return new tnDAquinoOpreaAIInputFiller(i_Character, i_Role);
    }
    public DAquinoOpreaAIFactory()
        : base()
    {
        m_Roles = new List<AIRole>();
    }

    // STATIC

}