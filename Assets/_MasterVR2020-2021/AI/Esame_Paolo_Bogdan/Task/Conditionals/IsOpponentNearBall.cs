﻿using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;
using UnityEngine;

using System.Collections.Generic;

public class IsOpponentNearBall : DaqOprBaseConditional
{
    public SharedTransform nearest;
    protected override TaskStatus Execute()
    {
        nearest.Value = null;
        float minDistance = float.MaxValue;

        for (int opponentIndex = 0; opponentIndex < m_taskData.input.opponentsCount; ++opponentIndex)
        {
            Transform opponent = m_taskData.input.m_Opponents[opponentIndex];
            if (opponent != null)
            {
                if (m_taskData.self || (opponent != m_taskData.self.transform))
                {
                    Vector2 opponentPosition = opponent.position;
                    Vector2 toTarget = new Vector2(m_taskData.input.ballPosition.x, m_taskData.input.ballPosition.y) - opponentPosition;
                    float distance = toTarget.magnitude;
                    if (distance <= minDistance)
                    {
                        nearest.Value = opponent;
                        minDistance = distance;
                    }
                }
            }
        }
        return nearest.Value == m_taskData.self.transform ? TaskStatus.Success : TaskStatus.Failure;

    }
}
