﻿using BehaviorDesigner.Runtime.Tasks;
using UnityEngine;

using System.Collections.Generic;
public class IsGoalInMyGKArea : DaqOprBaseConditional
{
    protected override TaskStatus Execute()
    {
        return IsInMyGKArea() ? TaskStatus.Success : TaskStatus.Failure;
    }
    protected bool IsInMyGKArea()
    {   
        Vector2 myGoalReference = m_taskData.GetFieldPosition(m_taskData.input.myGoal.position);

        if (myGoalReference.x < m_taskData.input.midfield.x) // Left goal.
        {
            return true;
        }
        else // Right goal.
        {
            return false;
        }
    }
}
