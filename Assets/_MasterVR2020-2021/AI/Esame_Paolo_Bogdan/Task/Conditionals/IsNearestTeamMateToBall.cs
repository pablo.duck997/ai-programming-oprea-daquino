﻿using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;
using UnityEngine;

using System.Collections.Generic;
public class IsNearestTeamMateToBall :DaqOprBaseConditional
{
    public SharedTransform nearest;
    protected override TaskStatus Execute()
    {
        nearest.Value = null;
        float minDistance = float.MaxValue;

        for (int teammateIndex = 0; teammateIndex < m_taskData.input.teamCharactersCount; ++teammateIndex)
        {
            Transform teammate = m_taskData.input.m_Teams[teammateIndex];
            if (teammate != null)
            {
                if (m_taskData.self || (teammate != m_taskData.self.transform))
                {
                    Vector2 teammatePosition = teammate.position;
                    Vector2 toTarget = new Vector2(m_taskData.input.ballPosition.x, m_taskData.input.ballPosition.y) - teammatePosition;
                    float distance = toTarget.magnitude;
                    if (distance <= minDistance)
                    {
                        nearest.Value = teammate;
                        minDistance = distance;
                    }
                }
            }
        }
        return nearest.Value == m_taskData.self.transform ? TaskStatus.Success : TaskStatus.Failure;

    }


}
