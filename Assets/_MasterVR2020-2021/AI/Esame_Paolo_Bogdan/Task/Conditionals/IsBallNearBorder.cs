﻿using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;

public class IsBallNearBorder : DaqOprBaseConditional
{
    public SharedFloat m_tolerance = 0.1f;
    protected override TaskStatus Execute()
    {
        float tolerance = m_taskData.input.colliderRadius;
        tolerance += m_tolerance.Value;
        return IsNearBorder(tolerance) ? TaskStatus.Success : TaskStatus.Failure;
    }
    private bool IsNearBorder(float i_tolerance)
    {
        bool result = false;
        if(m_taskData.IsCloseToLeftBorder(m_taskData.input.ballPosition, i_tolerance))
             return result=true;
        else if (m_taskData.IsCloseToRightBorder(m_taskData.input.ballPosition, i_tolerance))
            return result = true;
        else if(m_taskData.IsCloseToTopBorder(m_taskData.input.ballPosition, i_tolerance))
            return result = true;
        else if(m_taskData.IsCloseToBottomBorder(m_taskData.input.ballPosition, i_tolerance))
            return result = true;
        else
            return result;
    }
}
