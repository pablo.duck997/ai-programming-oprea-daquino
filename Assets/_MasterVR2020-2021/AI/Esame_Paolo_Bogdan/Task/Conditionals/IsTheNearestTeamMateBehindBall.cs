﻿using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;
using UnityEngine;

using System.Collections.Generic;
public class IsTheNearestTeamMateBehindBall : DaqOprBaseConditional
{
	protected override TaskStatus Execute()
	{
        Transform nearest = null;
        float minDistance = float.MaxValue;

        for (int teammateIndex = 0; teammateIndex < m_taskData.input.teamCharactersCount; ++teammateIndex)
        {
            Transform teammate = m_taskData.input.m_Teams[teammateIndex];
            if (teammate != null)
            {
                if (m_taskData.self || (teammate != m_taskData.self.transform))
                {
                    if (m_taskData.IsCharacterBehind(teammate))
                    {
                        Vector2 teammatePosition = teammate.position;
                        Vector2 toTarget = new Vector2(m_taskData.input.myGoal.position.x, m_taskData.input.myGoal.position.y) - teammatePosition;
                        float distance = toTarget.magnitude;
                        if (distance < minDistance)
                        {
                            nearest = teammate;
                            minDistance = distance;
                        }
                    }
                }
            }
        }
        return nearest == m_taskData.self.transform ? TaskStatus.Success : TaskStatus.Failure;
	}
}
