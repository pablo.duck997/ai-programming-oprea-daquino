﻿using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;
public class IsDirectionForced : DaqOprBaseConditional
{
    public SharedBool IsAttractionDirectionForced;
    protected override TaskStatus Execute()
    {
        //ballDistance < m_AttractMinRadius
        return IsAttractionDirectionForced.Value ? TaskStatus.Success : TaskStatus.Failure;
    }
}
