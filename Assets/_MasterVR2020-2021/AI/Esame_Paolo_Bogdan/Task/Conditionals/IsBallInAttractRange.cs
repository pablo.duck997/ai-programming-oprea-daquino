﻿using BehaviorDesigner.Runtime.Tasks;

public class IsBallInAttractRange :  DaqOprBaseConditional 
{
    protected override TaskStatus Execute()
    {   
        //ballDistance < m_AttractMinRadius
        return m_taskData.input.ballDistance < m_taskData.input.m_AttractMinRadius ? TaskStatus.Success : TaskStatus.Failure;
    }
}
