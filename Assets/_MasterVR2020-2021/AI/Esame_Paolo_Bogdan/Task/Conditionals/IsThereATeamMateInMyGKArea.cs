﻿using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;
using UnityEngine;

using System.Collections.Generic;

public class IsThereATeamMateInMyGKArea : DaqOprBaseConditional
{
    protected override TaskStatus Execute()
    {
        bool thereIs = false;
        foreach (Transform mate in m_taskData.input.m_Teams)
        {
            Vector2 matePos = mate.position;
            Vector2 myGoalReference = m_taskData.GetFieldPosition(m_taskData.input.myGoal.position);

            if (myGoalReference.x < m_taskData.input.midfield.x) // Left goal.
            {
                thereIs=(matePos.x < (myGoalReference.x + m_taskData.input.gkAreaWidth)) && (matePos.y > m_taskData.input.gkAreaMinHeight && matePos.y < m_taskData.input.gkAreaMaxHeight);
            }
            else
            {
                thereIs=(matePos.x > (myGoalReference.x - m_taskData.input.gkAreaWidth)) &&
                       (matePos.y > m_taskData.input.gkAreaMinHeight && matePos.y < m_taskData.input.gkAreaMaxHeight);
            }
        }
        return thereIs ? TaskStatus.Success : TaskStatus.Failure;
    }
}
