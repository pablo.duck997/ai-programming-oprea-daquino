﻿using BehaviorDesigner.Runtime.Tasks;
public class IsBehindBall : DaqOprBaseConditional
{
    protected override TaskStatus Execute()
    {
        return m_taskData.IsBehindBall(m_taskData.input.ballPosition) ? TaskStatus.Success : TaskStatus.Failure;
    }
}
