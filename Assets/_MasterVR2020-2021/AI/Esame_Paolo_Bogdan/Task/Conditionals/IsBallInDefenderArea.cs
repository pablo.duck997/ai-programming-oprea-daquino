﻿using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;
using UnityEngine;

using System.Collections.Generic;

public class IsBallInDefenderArea : DaqOprBaseConditional
{
	public SharedFloat Weight = 0.5f;
	protected override TaskStatus Execute()
	{
		Vector2 myGoalPos = new Vector2(m_taskData.input.myGoal.position.x, m_taskData.input.myGoal.position.y);
		float w = Mathf.Clamp01(Weight.Value);
		return m_taskData.IsNearTo(m_taskData.input.ballPosition, myGoalPos, Mathf.Abs(myGoalPos.x - m_taskData.input.midfield.x) * w) ? TaskStatus.Success : TaskStatus.Failure;
	}
}
