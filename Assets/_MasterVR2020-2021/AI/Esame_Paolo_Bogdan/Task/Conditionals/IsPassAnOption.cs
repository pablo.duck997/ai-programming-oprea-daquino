﻿using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;
using UnityEngine;

using System.Collections.Generic;
public class IsPassAnOption : DaqOprBaseConditional
{

    public SharedInt AllowedTeammatesToEvaluateCondition = 3;
    public SharedTransform nearest;
    protected override TaskStatus Execute()
    {
        if (m_taskData.input.teammatesCount < AllowedTeammatesToEvaluateCondition.Value) return TaskStatus.Success;
       
        nearest.Value = null;
        float minDistance = float.MaxValue;

        for (int teammateIndex = 0; teammateIndex < m_taskData.input.teamCharactersCount; ++teammateIndex)
        {
            Transform teammate = m_taskData.input.m_Teams[teammateIndex];
            if (teammate != null)
            {
                Vector2 teammatePosition = teammate.position;
                Vector2 toTarget = new Vector2(m_taskData.input.myGoal.position.x, m_taskData.input.myGoal.position.y) - teammatePosition;
                float distance = toTarget.magnitude;
                if (distance < minDistance)
                {
                    nearest.Value = teammate;
                    minDistance = distance;
                }
            }
        }
        return nearest.Value == m_taskData.self.transform ? TaskStatus.Failure : TaskStatus.Success;
    }
}
