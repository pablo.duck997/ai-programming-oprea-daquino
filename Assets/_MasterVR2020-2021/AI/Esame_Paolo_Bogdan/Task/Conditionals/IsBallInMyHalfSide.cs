﻿using BehaviorDesigner.Runtime.Tasks;
using UnityEngine;

using System.Collections.Generic;

public class IsBallInMyHalfSide : DaqOprBaseConditional
{
    protected override TaskStatus Execute()
    {
        float positionX = m_taskData.input.ballPosition.x;
        float myGoalX = m_taskData.input.myGoal.transform.position.x;
        float midfieldX = m_taskData.input.midfield.x;
        if ((myGoalX > midfieldX && positionX > midfieldX) || (myGoalX < midfieldX && positionX < midfieldX))
        {
            return TaskStatus.Success;
        }

        return TaskStatus.Failure;
    }

}
