﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;

  public abstract class DaqOprBaseConditional : BehaviorDesigner.Runtime.Tasks.Conditional
{
    protected DaqOprSheardTaskFunctions m_taskData = new DaqOprSheardTaskFunctions();
    public override void OnAwake()
    {
        base.OnAwake();
        m_taskData.Init(this);
    }

    public override void OnStart()
    {
        base.OnStart();
        m_taskData.OnStart();
    }

    public override TaskStatus OnUpdate()
     {
         if (!m_taskData.UpdateData()) return TaskStatus.Failure;
         return Execute();
     }

     protected abstract TaskStatus Execute();

}


