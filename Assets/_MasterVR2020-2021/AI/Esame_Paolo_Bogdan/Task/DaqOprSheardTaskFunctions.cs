﻿using System;
using System.Collections.Generic;
using DaqOpr_SharedVariables;
using UnityEngine;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;
using TrueSync;
public class DaqOprSheardTaskFunctions
{
    protected Task m_task;
    public GameObject self;
    public InputGameData input;
    public GameTimers timers;
    protected Behavior m_owner => m_task.Owner;

    public void Init(Task task)
    {
        m_task = task;
    }
    public virtual void OnStart()
    {
        self = m_owner.GetVariable("Self").GetValue() as GameObject;
    }
    public virtual bool UpdateData()
    {
        input = (InputGameData)m_owner.GetVariable("InputGameData").GetValue();
        timers = (GameTimers)m_owner.GetVariable("GameTimers").GetValue();
        return true;
    }

    public bool IsBehindBall(Vector2 ballPosition)
    {
        Vector2 myGoalPosition = input.myGoal.transform.position;
        Vector2 myPos = input.myPosition;
        return ((myGoalPosition.x > 0f && myPos.x > ballPosition.x) || (myGoalPosition.x < 0f && myPos.x < ballPosition.x));
    }

    public bool IsCharacterBehind(Transform character)
    {
        if (character == null)
        {
            return false;
        }

        if (input.ball == null || input.myGoal == null)
        {
            return false;
        }

        Vector2 ballPosition = input.ball.position;
        Vector2 myGoalPosition = input.myGoal.transform.position;

        float borderThreshold = 0.3f;//character colider radius
        borderThreshold += 0.5f; // Safety threshold.

        if (myGoalPosition.x > 0f)
        {
            return (character.position.x > ballPosition.x) || (input.bottomRight != null && (input.bottomRight.x - character.position.x < borderThreshold));
        }
        else
        {
            return (character.position.x < ballPosition.x) || (input.topLeft != null && (character.position.x - input.topLeft.x < borderThreshold));
        }
    }

    public bool IsNearTo(Vector2 what, Vector2 pointOfIntrest, float offset = 0)
    {
        Vector2 position = GetFieldPosition(pointOfIntrest);

        if (position.x < input.midfield.x) // Left goal.
        {
            return (what.x < (position.x + offset));
        }
        else // Right goal.
        {
            return (what.x > (position.x - offset));
        }
    }
    public Vector2 GetFieldPosition(Vector2 i_Position)
    {
        float halfFieldWidth = input.fieldWidth / 2.0f;
        float halfFieldHeight = input.fieldHeight / 2.0f;

        // Check left.

        float leftThreshold = input.midfield.x - halfFieldWidth;

        if (i_Position.x < leftThreshold)
        {
            i_Position.x = leftThreshold;
        }

        // Check right.

        float rightThreshold = input.midfield.x + halfFieldWidth;

        if (i_Position.x > rightThreshold)
        {
            i_Position.x = rightThreshold;
        }

        // Check top.

        float topThreshold = input.midfield.y + halfFieldHeight;

        if (i_Position.y > topThreshold)
        {
            i_Position.y = topThreshold;
        }

        // Check bottom.

        float bottomThreshold = input.midfield.y - halfFieldHeight;

        if (i_Position.y < bottomThreshold)
        {
            i_Position.y = bottomThreshold;
        }

        return i_Position;
    }

    public bool IsCloseToLeftBorder(Vector2 i_Position, float i_Tolerance)
    {
        return (i_Position.x - input.topLeft.x < i_Tolerance);
    }
    public bool IsCloseToRightBorder(Vector2 i_Position, float i_Tolerance)
    {
        return (input.bottomRight.x - i_Position.x < i_Tolerance);
    }
    public bool IsCloseToTopBorder(Vector2 i_Position, float i_Tolerance)
    {
        return (input.topLeft.y - i_Position.y < i_Tolerance);
    }
    public bool IsCloseToBottomBorder(Vector2 i_Position, float i_Tolerance)
    {
        return (i_Position.y - input.bottomRight.y < i_Tolerance);
    }
    public Vector2 GetMyGoalDirection(Vector2 i_from)
    {
        if (input.myGoal == null)
        {
            return Vector2.zero;
        }

        Vector2 toMyGoal = new Vector2(input.myGoal.position.x, input.myGoal.position.y) - i_from;
        Vector2 toMyGoalDirection = toMyGoal.normalized;

        return toMyGoalDirection;
    }
    public Vector2 GetBallDirection(Vector2 i_From)
    {
        Vector2 ballPositionVec2 = input.ballPosition;
        Vector2 toBall = ballPositionVec2 - i_From;
        Vector2 toBallDirection = toBall.normalized;

        return toBallDirection;
    }

    public Vector2 ClampPosition(Vector2 i_Position, float i_Tolerance = 0f)
    {
        // Check left.
        float halfFieldWidth = input.fieldWidth / 2.0f;
        float halfFieldHeight = input.fieldHeight / 2.0f;

        float leftThreshold = input.midfield.x - halfFieldWidth;
        leftThreshold += i_Tolerance;

        if (i_Position.x < leftThreshold)
        {
            i_Position.x = leftThreshold;
        }

        // Check right.

        float rightThreshold = input.midfield.x + halfFieldWidth;
        rightThreshold -= i_Tolerance;

        if (i_Position.x > rightThreshold)
        {
            i_Position.x = rightThreshold;
        }

        // Check top.

        float topThreshold = input.midfield.y + halfFieldHeight;
        topThreshold -= i_Tolerance;

        if (i_Position.y > topThreshold)
        {
            i_Position.y = topThreshold;
        }

        // Check bottom.

        float bottomThreshold = input.midfield.y - halfFieldHeight;
        bottomThreshold += i_Tolerance;

        if (i_Position.y < bottomThreshold)
        {
            i_Position.y = bottomThreshold;
        }

        return i_Position;
    }

    public Vector2 Seek(Vector2 i_Target, float i_Tolerance = 0f)
    {
        if (self == null)
        {
            return Vector2.zero;
        }

        Vector2 difference = i_Target - input.myPosition;

        if (difference.sqrMagnitude < i_Tolerance * i_Tolerance)
        {
            return Vector2.zero;
        }

        Vector2 direction = difference.normalized;
        return direction;
    }
    public Vector2 SeekPosition(Vector2 i_Position, float i_Tolerance)
    {
        Vector2 target;
        ComputeFieldPosition(out target);

        target += i_Position;
        target *= 0.5f;

        Vector2 axes = Seek(target, i_Tolerance);
        return axes;
    }
    public Vector2 GetVelocity(GameObject gameObj)
    {
        if (gameObj == null)
        {
            return Vector2.zero;
        }

        TSRigidBody2D rigidbody2d = gameObj.GetComponent<TSRigidBody2D>();
        if (rigidbody2d == null) return Vector2.zero;
        TSVector2 tsVelocity = rigidbody2d.velocity;
        Vector2 velocity = tsVelocity.ToVector();
        return velocity;
    }
    public float GetSpeed(GameObject gameObj)
    {
        if (gameObj is null) return 0f;
        TSRigidBody2D rigidbody2d = gameObj.GetComponent<TSRigidBody2D>();
        if (rigidbody2d == null) return 0f;
        TSVector2 tsVelocity = rigidbody2d.velocity;
        FP tsSpeed = tsVelocity.magnitude;
        float speed = tsSpeed.AsFloat();
        return speed;
    }
    public void  ComputeGkPosition(out Vector2 o_Position)
    {
        Vector2 gkPosition = GetFieldPosition(new Vector2(input.myGoal.position.x, input.myGoal.position.y));

        float idealHeight = input.ballPosition.y;

        Vector2 ballVelocity = GetVelocity(input.ball.gameObject);
        if (ballVelocity.magnitude > 0.1f)
        {
            Vector2 ballDirection = ballVelocity.normalized;
            Vector2 ballToGoalDirection = GetMyGoalDirection(new Vector2(input.ball.position.x, input.ball.position.y));

            float dot = Vector2.Dot(ballDirection, ballToGoalDirection);

            if (dot > 0f)
            {
                idealHeight = input.ball.position.y + (gkPosition.x - input.ball.position.x) * (ballVelocity.y / ballVelocity.x);
            }
        }

        float goalMinHeight = gkPosition.y - input.goalWidth / 2f;
        float goalMaxHeight = gkPosition.y + input.goalWidth / 2f;

        idealHeight = Mathf.Clamp(idealHeight, goalMinHeight, goalMaxHeight);

        if (gkPosition.x > input.midfield.x) // Right goal.
        {
            gkPosition.x -= input.colliderRadius;
            gkPosition.x -= 0.15f;
        }
        else // Left goal.
        {
            gkPosition.x += input.colliderRadius;
            gkPosition.x += 0.15f;
        }

        gkPosition.y = idealHeight;

        o_Position = gkPosition;
    }
    public void ComputeFieldPosition(out Vector2 o_Position)
    {
        Vector2 target = Vector2.zero;

        // Compute sepration.

        {
            Vector2 separationTarget;
            ComputeSeparationTarget(out separationTarget);

            target += separationTarget;
        }

        // Seek/Flee ball.

        {
            float fleeMinDistance = input.fieldHeight * input.m_MinFleeDistanceFactor;
            if (input.ballDistance < fleeMinDistance)
            {
                Vector2 seekTarget = input.myPosition;
                ComputeFleeTarget(input.ballPosition, fleeMinDistance, out seekTarget);

                target += seekTarget;
                target *= 0.5f;
            }
            else
            {
                float fleeMaxDistance = input.fieldHeight * input.m_MaxFleeDistanceFactor;
                if (input.ballDistance > fleeMaxDistance)
                {
                    Vector2 seekTarget = input.myPosition;
                    ComputeSeekTarget(input.ballPosition, fleeMaxDistance, out seekTarget);

                    target += seekTarget;
                    target *= 0.5f;
                }
            }
        }

        o_Position = target;
    }
    void ComputeSeekTarget(Vector2 i_Source, float i_Threshold, out Vector2 o_Target)
    {
        Vector2 selfToSource = i_Source - input.myPosition;
        float distanceToSource = selfToSource.magnitude;

        Vector2 selfToSourceDirection = selfToSource.normalized;

        o_Target = input.myPosition + selfToSourceDirection * (distanceToSource - i_Threshold);
    }

    private void ComputeFleeTarget(Vector2 i_Source, float i_Threshold, out Vector2 o_Target)
    {
        Vector2 sourceToSelfDirection = input.myPosition - i_Source;
        sourceToSelfDirection.Normalize();

        o_Target = i_Source + sourceToSelfDirection * i_Threshold;
    }

    private void ComputeSeparationTarget(out Vector2 o_Target)
    {
        Vector2 sum = Vector2.zero;
        int count = 0;

        for (int teammateIndex = 0; teammateIndex < input.teammatesCount; ++teammateIndex)
        {
            Transform teammate = GetTeammateByIndex(teammateIndex);
            if (teammate != null)
            {
                Vector2 teammatePosition = teammate.position;
                Vector2 teammateToSelf = input.myPosition - teammatePosition;

                float distance = teammateToSelf.magnitude;

                if (distance < input.m_SeparationThreshold)
                {
                    Vector2 teammateToSelfDirection = teammateToSelf.normalized;

                    Vector2 target = teammatePosition + teammateToSelfDirection * input.m_SeparationThreshold;
                    sum += target;

                    ++count;
                }
            }
        }

        if (count > 0)
        {
            Vector2 separationTarget = sum / count;
            o_Target = separationTarget;
        }
        else
        {
            o_Target = input.myPosition;
        }
    }
    private Transform GetTeammateByIndex(int i_Index)
    {
        if (i_Index < 0 || i_Index >= input.m_Teams.Count)
        {
            return null;
        }

        return input.m_Teams[i_Index];
    }
    public Transform GetOpponentNearestTo(Vector3 position, List<Transform> opponents)
    {
        Transform nearest = null;
        float minDistance = float.MaxValue;

        foreach (Transform opponent in opponents)
        {
            if (opponent == null) continue;

            Vector2 opponentPosition = opponent.position;
            Vector2 toTarget = (Vector2)position - opponentPosition;
            float distance = toTarget.magnitude;

            if (!(distance <= minDistance)) continue;
            nearest = opponent;
            minDistance = distance;
        }

        return nearest;
    }
}

