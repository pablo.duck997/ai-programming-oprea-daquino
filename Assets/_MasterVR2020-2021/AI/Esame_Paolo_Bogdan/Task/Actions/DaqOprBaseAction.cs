﻿using System;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;
using UnityEngine;
using DaqOpr_SharedVariables;

public abstract class DaqOprBaseAction : BehaviorDesigner.Runtime.Tasks.Action
{     // -------- OUTPUT
    private DaqOprOutput shared_Output;

    protected DaqOprSheardActionTaskFunctionscript m_actionData = new DaqOprSheardActionTaskFunctionscript();
    public override void OnAwake()
    {
        base.OnAwake();
        m_actionData.Init(this);
    }
    public override void OnStart()
    {
        base.OnStart();
        shared_Output = (DaqOprOutput)Owner.GetVariable("OutputGameData");
        m_actionData.OnStart();
    }

    public override TaskStatus OnUpdate()
    {
        if (!m_actionData.UpdateData()) return TaskStatus.Failure;

        TaskStatus result;
        try
        {
            result = Execute();
          
        }
        catch (Exception e)
        {
            Debug.LogWarning(e.Message);
            return TaskStatus.Failure;
        }

        if (m_actionData.output.kickButton)
            RegularizeKick();
        if (m_actionData.output.dashButton)
            RegularizeDash();
        if (m_actionData.output.tackleRequested)
            RegularizeTackle();
        if (!m_actionData.output.isAttracting)
            RegularizeAttract();

        PatchData();
        return result;
    }

    protected abstract TaskStatus Execute();
    private void PatchData()
    {
        shared_Output.SetValue(m_actionData.output);
    }

    private void RegularizeAttract()
    {
        if (!m_actionData.output.isAttracting && m_actionData.timers.attractTimer > 0f)
        {
            m_actionData.timers.attractCooldownTimer = m_actionData.input.m_AttractCooldown;
        }
    }

    private void RegularizeTackle()
    {
        if (!m_actionData.input.isEnergyEnoughToTackle
            || m_actionData.timers.tackleCooldownTimer > 0f)
        {
            // cannot perform action
            m_actionData.output.tackleRequested = false;
        }
        else
        {
            m_actionData.timers.tackleCooldownTimer = m_actionData.input.m_TackleCooldown;
        }
    }

    private void RegularizeKick()
    {
        if (!m_actionData.input.isEnergyEnoughToKick
            || m_actionData.timers.kickCooldownTimer > 0f)
        {
            // cannot perform action
            m_actionData.output.kickButton = false;
        }
        else
        {
            m_actionData.timers.kickCooldownTimer = m_actionData.input.m_KickCooldown;
        }
    }

    private void RegularizeDash()
    {
        if (!m_actionData.input.isEnergyEnoughToDash
            || m_actionData.timers.dashCooldownTimer > 0f)
        {
            // cannot perform action
            m_actionData.output.dashButton = false;
        }
        else
        {
            m_actionData.timers.dashCooldownTimer = m_actionData.input.m_DashCooldown;
        }
    }
}
