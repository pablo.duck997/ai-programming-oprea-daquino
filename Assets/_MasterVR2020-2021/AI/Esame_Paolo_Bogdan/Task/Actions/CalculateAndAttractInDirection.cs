﻿using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;
using UnityEngine;
using DaqOpr_SharedVariables;
public class CalculateAndAttractInDirection : DaqOprBaseAction
{
	protected override TaskStatus Execute()
	{
		Vector2 selfPosition = m_actionData.input.myPosition;

		Vector2 myGoalDirection = m_actionData.GetMyGoalDirection(selfPosition);
		Vector2 ballDirection = m_actionData.GetBallDirection(selfPosition);

		Vector2 axisA = new Vector2(ballDirection.y, -ballDirection.x);
		Vector2 axisB = -axisA;

		float dotA = Vector2.Dot(axisA, myGoalDirection);
		float dotB = Vector2.Dot(axisB, myGoalDirection);

		m_actionData.output = new OutputGameDataBuilder().WithAxes(dotA > dotB ? axisA : axisB).WithAttract(true).Build();

		return TaskStatus.Success;
	}
}
