﻿using BehaviorDesigner.Runtime.Tasks;
using UnityEngine;
using DaqOpr_SharedVariables;

public class WaitForBall : DaqOprBaseAction
{
    protected override TaskStatus Execute()
    {
		Vector2 strikerPosition = m_actionData.input.referencePos;
		strikerPosition.x = m_actionData.input.midfield.x - (m_actionData.input.referencePos.x - m_actionData.input.midfield.x);

		m_actionData.output = new OutputGameDataBuilder().WithAxes(m_actionData.SeekPosition(strikerPosition, m_actionData.input.colliderRadius)).Build();
		return TaskStatus.Running;
	}
}

