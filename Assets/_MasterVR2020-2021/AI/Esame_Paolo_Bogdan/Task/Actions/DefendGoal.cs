﻿using BehaviorDesigner.Runtime.Tasks;
using UnityEngine;
using DaqOpr_SharedVariables;

public class DefendGoal : DaqOprBaseAction
{
    protected override TaskStatus Execute()
    {
        // Protect your goal!
        m_actionData.ComputeGkPosition(out Vector2 gkPosition);
        Vector2 axes = m_actionData.Seek(gkPosition, m_actionData.input.colliderRadius);
        bool kickButton = false;
        // If you can kick the ball away, try to do it.
        float kickRadius = m_actionData.input.colliderRadius;
        kickRadius += m_actionData.input.ballRadius;
        kickRadius += 0.25f; // Safety threshold.

        if (m_actionData.input.ballDistance < kickRadius)
        {
            kickButton = true;
        }

        m_actionData.output =new OutputGameDataBuilder().WithAxes(axes).WithKick(kickButton).Build();

        return TaskStatus.Running;
    }
}
