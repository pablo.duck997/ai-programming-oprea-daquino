﻿using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;
using UnityEngine;
using DaqOpr_SharedVariables;

public class Interpose : DaqOprBaseAction
{
    public SharedFloat Weight = 0.5f;
    protected override TaskStatus Execute()
    {
        float w = Mathf.Clamp01(Weight.Value);

        Vector2 myPosition = m_actionData.self.transform.position;
        Vector2 aPosition = m_actionData.input.ballPosition;
        Vector2 bPosition = m_actionData.input.myGoal.position;

        Vector2 aVelocity = m_actionData.GetVelocity(m_actionData.input.ball.gameObject);

        float mySpeed = m_actionData.GetSpeed(m_actionData.self);

        Vector2 midPoint = Vector2.Lerp(aPosition, bPosition, w);

        Vector2 toMidPoint = midPoint - myPosition;
        float midPointDistance = toMidPoint.magnitude;

        float timeToReachMidPoint = 0f;
        if (mySpeed > Mathf.Epsilon)
        {
            timeToReachMidPoint = midPointDistance / mySpeed;
        }

        Vector2 aFuturePosition = aPosition + aVelocity * timeToReachMidPoint;
        Vector2 bFuturePosition = bPosition;

        Vector2 futureMidPoint = Vector2.Lerp(aFuturePosition, bFuturePosition, w);

        m_actionData.output = new OutputGameDataBuilder().WithAxes(m_actionData.Seek(futureMidPoint)).Build();

        return TaskStatus.Running;
    }
}
