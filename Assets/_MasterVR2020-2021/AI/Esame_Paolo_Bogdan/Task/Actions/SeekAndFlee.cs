﻿using BehaviorDesigner.Runtime.Tasks;
using UnityEngine;
using DaqOpr_SharedVariables;

public class SeekAndFlee : DaqOprBaseAction
{
    protected override TaskStatus Execute()
    {
        m_actionData.ComputeFieldPosition(out Vector2 target);
        m_actionData.output = new OutputGameDataBuilder().WithAxes(m_actionData.Seek(target, m_actionData.input.colliderRadius)).Build();
        return TaskStatus.Running;
    }
}
