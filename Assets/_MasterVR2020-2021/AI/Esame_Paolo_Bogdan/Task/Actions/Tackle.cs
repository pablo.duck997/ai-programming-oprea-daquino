﻿using BehaviorDesigner.Runtime.Tasks;
using DaqOpr_SharedVariables;
using UnityEngine;


public class Tackle : DaqOprBaseAction
{
    public override void OnAwake()
    {
        base.OnAwake();
        m_actionData.log = false;
    }

    protected override TaskStatus Execute()
    {
        if (m_actionData.output.kickButton || m_actionData.input.m_Opponents == null) return TaskStatus.Failure;

        // get the nearest opponent
        Transform nearestOpponent = m_actionData.GetOpponentNearestTo(m_actionData.input.myPosition, m_actionData.input.m_Opponents);

        // check the distance

        Vector2 nearestOpponentPos = nearestOpponent.position;
        float distance = Vector2.Distance(m_actionData.input.myPosition, nearestOpponentPos);

        bool tackleRequested = distance < m_actionData.input.m_TackleRadius && m_actionData.input.ballDistance > m_actionData.input.m_BallDistanceThreshold;

        m_actionData.output =new OutputGameDataBuilder().WithTackle(tackleRequested).Build();

        return TaskStatus.Success;
    }
}
