﻿using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;
public class AttractBall : DaqOprBaseAction
{
    public SharedBool IsDirectionForced;
    protected override TaskStatus Execute()
    {
        IsDirectionForced.Value = false;
        return m_actionData.output.isAttracting ? TaskStatus.Running : TaskStatus.Failure;
    }
}
