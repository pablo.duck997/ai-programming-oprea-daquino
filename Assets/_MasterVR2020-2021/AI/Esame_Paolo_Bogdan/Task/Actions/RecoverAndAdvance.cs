﻿using BehaviorDesigner.Runtime.Tasks;
using UnityEngine;
using DaqOpr_SharedVariables;
public class RecoverAndAdvance : DaqOprBaseAction
{
    protected override TaskStatus Execute()
    {
        Vector2 position = new Vector2(m_actionData.input.opponentGoal.position.x, m_actionData.input.opponentGoal.position.y);
        Vector2 targetDistance = position - m_actionData.input.myPosition;
        Vector2 targetDirection = targetDistance.normalized;

        {
            // Apply direction correction in order to avoid the ball.

            Vector2 pointA = m_actionData.input.ballPosition;
            pointA.y += m_actionData.input.ballRadius;
            pointA.y += m_actionData.input.colliderRadius;
            pointA.y += 0.05f; // Safety tolerance.

            Vector2 pointB = m_actionData.input.ballPosition;
            pointB.y -= m_actionData.input.ballRadius;
            pointB.y -= m_actionData.input.colliderRadius;
            pointB.y -= 0.05f; // Safety tolerance.

            Vector2 selfToA = pointA - m_actionData.input.myPosition;
            Vector2 selfToB = pointB - m_actionData.input.myPosition;

            Vector2 directionA = selfToA.normalized;
            Vector2 directionB = selfToB.normalized;

            float angleA = Vector2.Angle(targetDirection, directionA);
            float angleB = Vector2.Angle(targetDirection, directionB);

            float angleAB = Vector2.Angle(directionA, directionB);

            if (angleA < angleAB && angleB < angleAB)
            {
                // Target direction is into ball fov. Apply a correction using the shortest line.
                targetDirection = selfToA.sqrMagnitude < selfToB.sqrMagnitude ? directionA : directionB;
            }
            else
            {
                // Target direction is fine. Nothing to do.
            }
        }

        Vector2 axes = targetDirection.normalized;
        bool dashButton = targetDistance.sqrMagnitude > (m_actionData.input.m_ForcedDashDistance * m_actionData.input.m_ForcedDashDistance);

        m_actionData.output = new OutputGameDataBuilder().WithAxes(axes).WithDash(dashButton).Build();

        return TaskStatus.Running;
    }
}
