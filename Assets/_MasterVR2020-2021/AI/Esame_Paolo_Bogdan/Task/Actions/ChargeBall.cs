﻿using BehaviorDesigner.Runtime.Tasks;
using UnityEngine;
using DaqOpr_SharedVariables;

public class ChargeBall : DaqOprBaseAction
{
    protected override TaskStatus Execute()
    {
        Vector2 axes = Vector2.zero;
        bool dashButton = false;
        bool kickButton = false;

        Vector2 target = new Vector2(m_actionData.input.ballPosition.x, m_actionData.input.ballPosition.y);
        Vector2 direction = target - new Vector2(m_actionData.input.opponentGoal.position.x, m_actionData.input.opponentGoal.position.y);

        direction.Normalize();

        float offset = m_actionData.input.ballRadius;
        offset += m_actionData.input.colliderRadius;
        offset += 0.05f; // Tolerance threshold.

        target += direction * offset;
        target = m_actionData.ClampPosition(target, 0.2f);
        axes = m_actionData.Seek(target);

        float targetDistance = Vector2.Distance(target, m_actionData.input.myPosition);

        if (m_actionData.timers.recoverTimer > m_actionData.input.m_RecoverTimeThreshold)
        {
            kickButton = true; // Force kick to recover.
        }
        else
        {
            if (targetDistance > m_actionData.input.m_DashDistance)
            {
                dashButton = true;
            }
            else if (targetDistance <= m_actionData.input.m_KickPrecision)
            {
                kickButton = true;
            }
        }

        m_actionData.output = new OutputGameDataBuilder().WithAxes(axes).WithDash(dashButton).WithKick(kickButton).Build();

        return TaskStatus.Running;

    }

}
