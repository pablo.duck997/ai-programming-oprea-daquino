﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using BehaviorDesigner.Runtime.Tasks;
using DaqOpr_SharedVariables;
public class DaqOprSheardActionTaskFunctionscript : DaqOprSheardTaskFunctions
{
    public OutputGameData output = new OutputGameData();

    public bool log = true;

    public override void OnStart()
    {
        base.OnStart();
    }

    public override bool UpdateData()
    {
        base.UpdateData();
        output = (OutputGameData)m_owner.GetVariable("OutputGameData").GetValue();
        if (output == null) throw new Exception("OutputGameData is null!");

        return true;
    }

}
